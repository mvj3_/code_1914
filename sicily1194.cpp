//题目网址: 
//http://soj.me/1194

//题目分析:
//有一个新知识点，关于set的用法，有序且不重复。
//不过由于输入大小不区分，所以要统一转换成小写再做处理
//set的输入操作是insert，擦除操作是erase

#include <iostream>
#include <string>
#include <set>

using namespace std;

string friends;
string senders;

int main()
{
    int n, m;
    set<string> names;
    
    while (cin >> n >> m && n != 0) {
        int i, j, l;

        for (i = 0; i < n; i++) { 
            cin >> friends;
            for (l = 0; l < friends.length(); l++)
                friends[l] = tolower(friends[l]);
            names.insert(friends);
        }

        for (j = 0; j < m; j++) { 
            cin >> senders;
            for (l = 0; l < senders.length(); l++)
                senders[l] = tolower(senders[l]);
            names.erase(senders);
        }

        cout << names.size() << endl;
        names.clear();
    }
    
    return 0;
}                                 